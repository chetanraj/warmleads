const mongoose = require("mongoose");

const MonthSchema = new mongoose.Schema({
  name: String,
  leads: Number,
  emailsOpened: String,
  mailsPerLead: Number,
});

const DashboardSchema = new mongoose.Schema({
  months: [MonthSchema],
});

const Dashboard = mongoose.model("Dashboard", DashboardSchema);

module.exports = Dashboard;
