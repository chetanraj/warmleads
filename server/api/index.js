const express = require("express");
const cors = require("cors");

const db = require("./db/connection")();

//! Schema
const Dashboard = require("./db/schema/Dashboard");

const app = express();
app.use(cors());

app.get("/api", async (req, res) => {
  const dashboard = await Dashboard.findOne({});

  res.send(dashboard).status(200);
});

app.get("/api/dashboard", async (req, res) => {
  const dashboard = await Dashboard.findOne({});

  res.send(dashboard).status(200);
});

//! LOCAL
// const port = process.env.PORT || 8080;
// app.listen(port, () => console.log(`Listening on port ${port}..`));

//! Vercel
module.exports = app;
