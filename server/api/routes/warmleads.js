const express = require("express");

//! Schema
const Dashboard = require("../db/schema/Dashboard");

const router = express.Router();



router.get("/dashboard", async (req, res) => {
  const dashboard = await Dashboard.find({});

  res.send(dashboard).status(200);
});

module.exports = router;
