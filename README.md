# Warmleads

### List of improvements

Backend
- [ ] Secure the password of the MongoDB Cloud in an `.env` file
- [ ] More structured collections in MongoDB, like making the Schema more sophisticated in Date range so we can represent them in better way in frontend. Capture the Lead contact details & create a service to contact them

Frontend
- [ ] Re-organise to a better folder structure
- [ ] Show charts for monthly lead progress & Opened emails
- [ ] The components `Dashboard.js` & `Monthly.js` has some of repeated code & the state is sharing the same data. Will be using a state management system to re-use data aroundf the app
- [ ] Make the Range filter data better, not showing month specific, rather than showing Date range
- [ ] Take actions to the lead from the Lead table - Contact the leads, View more details of the lead
- [ ] Login, Logout, Session & whole nine yards of authentication
