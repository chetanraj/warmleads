import React from "react";

const Loader = () => {
  return (
    <div className="p-6 flex items-center justify-center">
      <div className="loader">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Loader;
