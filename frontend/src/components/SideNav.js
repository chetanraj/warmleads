import React from "react";
import { Link } from "react-router-dom";
import { Home, Calendar } from "react-feather";

import Logo from "./Logo";

const links = [
  {
    label: "Dashboard",
    key: "dashboard",
    link: "/",
    icon: "Home",
  },
  {
    label: "Monthly",
    key: "monthly",
    link: "/monthly",
    icon: "Calendar",
  },
];

const icons = { Home, Calendar };

const SideNav = ({ currentPage }) => {
  return (
    <div className="sidenav text-xl">
      <div className="text-center pt-4">
        <a className="text-gray-600" href="/">
          <Logo />
        </a>
      </div>
      <div className="">
        <aside className="flex flex-col h-screen px-5 py-8 overflow-y-auto bg-white">
          <nav className="">
            <div className="space-y-3 ">
              {links.map(({ label, key, link, icon }) => {
                const Icon = icons[icon];

                return (
                  <Link
                    className={`flex items-center px-3 py-2 text-gray-600 transition-colors duration-300 transform rounded-lg hover:bg-indigo-500 hover:text-white ${
                      key === currentPage ? "bg-indigo-500 text-white" : ""
                    }`}
                    to={link}
                  >
                    <Icon />
                    <span className="mx-2 text-xl font-medium">{label}</span>
                  </Link>
                );
              })}
            </div>
          </nav>
        </aside>
      </div>
    </div>
  );
};

export default SideNav;
