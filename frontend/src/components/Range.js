import React, { useState } from "react";

const Range = ({ currentMonth, months, changeCurrentMonth }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="flex justify-between mx-6 mt-6">
      <div className="">
        Current period ({currentMonth.name || "Last 30 Days"})
      </div>
      <div className="relative inline-block text-left">
        <button
          type="button"
          className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 "
          onClick={() => setIsOpen(!isOpen)}
        >
          Range
          <svg
            className="-mr-1 h-5 w-5 text-gray-400"
            viewBox="0 0 20 20"
            fill="#4F46E5"
          >
            <path
              fillRule="evenodd"
              d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
              clipRule="evenodd"
            />
          </svg>
        </button>

        <div
          className={`absolute right-0 z-10 mt-2 w-32 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 ${
            isOpen ? "" : "hidden"
          }`}
        >
          <div className="py-1" role="none">
            {months.map((month) => {
              return (
                <button
                  onClick={() => {
                    changeCurrentMonth(month);
                    setIsOpen(false);
                  }}
                  className={`text-gray-700 block px-4 py-2 text-sm hover:bg-gray-100 hover:text-gray-900 w-full text-left ${
                    month.name === currentMonth.name
                      ? "bg-gray-100 text-gray-900"
                      : ""
                  }`}
                >
                  {month.name}
                </button>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Range;
