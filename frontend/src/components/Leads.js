import React from "react";

import Loader from "./Loader";

const Leads = ({ header = "Leads", leads, loading }) => {
  return (
    <div className="border border-slate-200 shadow-lg rounded m-6">
      <header class="px-5 py-4 border-b border-slate-100 flex items-center justify-between">
        <h2 class="font-semibold text-slate-800 text-xl">{header}</h2>
        <div>
          <div>🔥 - Lead is warm, there's been a reply from the customer</div>
          <div className="mt-2">
            🧊 - Lead is cold, no reply from the customer
          </div>
        </div>
      </header>

      {loading ? (
        <div>
          <Loader />
        </div>
      ) : (
        <>
          <div class="grow flex flex-col justify-center">
            <table class="border-collapse table-auto text-sm m-6">
              <thead>
                <tr>
                  <th className="border-b font-medium p-4 pt-0 pb-3 text-slate-400 text-left">
                    Name
                  </th>
                  <th className="border-b font-medium p-4 pt-0 pb-3 text-slate-400 text-left">
                    Company
                  </th>
                  <th className="border-b font-medium p-4 pt-0 pb-3 text-slate-400 text-left">
                    Phone
                  </th>
                  <th className="border-b font-medium p-4 pt-0 pb-3 text-slate-400 text-left">
                    Lead
                  </th>
                  <th className="border-b font-medium p-4 pt-0 pb-3 text-slate-400 text-left">
                    Response
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white">
                {leads.map(({ name, company, phone, isLeadHot, response }) => {
                  return (
                    <>
                      <tr>
                        <td className="border-b border-slate-100 p-4 text-slate-500">
                          {name}
                        </td>
                        <td className="border-b border-slate-100 p-4 text-slate-500">
                          {company}
                        </td>
                        <td className="border-b border-slate-100 p-4 text-slate-500">
                          {phone}
                        </td>
                        <td className="border-b border-slate-100 p-4 text-slate-500">
                          {isLeadHot ? "🔥" : "🧊"}
                        </td>
                        <td className="border-b border-slate-100 p-4 text-slate-500">
                          {response ? response : "N/A"}
                        </td>
                      </tr>
                    </>
                  );
                })}
              </tbody>
            </table>
          </div>
        </>
      )}
    </div>
  );
};

export default Leads;
