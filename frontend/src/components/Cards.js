import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Mail, Send, Users } from "react-feather";

const icons = { Mail, Send, Users };

const Cards = ({ count, label, icon, options, progress }) => {
  //! Local state
  const [isOpen, setIsOpen] = useState(false);

  const Icon = icons[icon];

  return (
    <div className="relative border border-slate-200 shadow-lg rounded px-6 py-6">
      {options && (
        <>
          <div className="absolute right-2 flex justify-end mb-6">
            <button
              className="text-slate-400 hover:text-slate-500 rounded-full"
              onClick={() => setIsOpen(!isOpen)}
            >
              <svg className="w-8 h-8 fill-current" viewBox="0 0 32 32">
                <circle cx="16" cy="16" r="2"></circle>
                <circle cx="10" cy="16" r="2"></circle>
                <circle cx="22" cy="16" r="2"></circle>
              </svg>
            </button>
            <div
              className={`origin-top-right z-10 absolute top-full right-0 w-40 bg-white border border-slate-200 py-1.5 rounded shadow-lg overflow-hidden mt-1 ${
                isOpen ? "" : "hidden"
              }`}
            >
              {options.map(({ link, label }) => {
                return (
                  <Link
                    to={link}
                    className="text-gray-700 block px-4 py-2 text-sm hover:bg-gray-100 hover:text-gray-900 w-full text-left"
                  >
                    {label}
                  </Link>
                );
              })}
            </div>
          </div>
        </>
      )}

      <Icon size={32} color="#4F46E5" />
      <div className="pt-16 transition-all flex items-center">
        <div className="text-3xl">{count}</div>
        {progress && (
          <div
            class={`text-sm font-semibold text-white px-1.5  rounded-full ml-2 ${
              progress.includes("+") ? "bg-green-500" : "bg-yellow-500"
            }`}
          >
            {progress}
          </div>
        )}
      </div>
      <div className="text-l text-gray-500">{label}</div>
    </div>
  );
};

export default Cards;
