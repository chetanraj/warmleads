import React from "react";

import SideNav from "./SideNav";
import Dashboard from "../pages/Dashboard";

export default function Main() {
  return (
    <div className="wrapper h-screen">
      <SideNav />
      <Dashboard />
    </div>
  );
}
