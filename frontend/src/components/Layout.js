import React from "react";
import { Bell } from "react-feather";

import SideNav from "./SideNav";

function Layout({ currentPage, children }) {
  return (
    <div className="wrapper h-screen">
      <SideNav currentPage={currentPage} />
      <div className="border-l border-gray-300">
        <div className="header border-b border-gray-300 flex items-center justify-between p-4">
          <div className="search">
            <div className="relative mx-auto text-gray-600">
              <input
                className="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                type="search"
                name="search"
                placeholder="Search"
              />
            </div>
          </div>
          <div className="flex h-12 items-center">
            <div className="flex">
              <button className="w-8 h-8 flex items-center justify-center bg-slate-100 hover:bg-slate-200 transition duration-150 rounded-full ml-3">
                <Bell size={18} />
              </button>
            </div>
            <hr className="w-px h-12 bg-slate-200 mx-3" />
            <div className="profile">
              <div className="bg-indigo-600 h-8 w-8 rounded-full text-center text-white font-bold p-1">
                J
              </div>
            </div>
          </div>
        </div>
        {children}
      </div>
    </div>
  );
}

export default Layout;
