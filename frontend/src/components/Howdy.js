import React from "react";

export default function Howdy() {
  return (
    <div className="relative bg-indigo-200 p-4 sm:p-6 rounded-sm overflow-hidden mb-8 m-6">
      <div className="relative">
        <h1 className="text-2xl md:text-3xl text-slate-800 font-bold mb-1">
          Howdy, John Doe 👋
        </h1>
        <p>
          Here is what’s happening with your leads today, keep it going while
          it's still warm
        </p>
      </div>
    </div>
  );
}
