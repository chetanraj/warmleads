import React, { useEffect, useState } from "react";

import Layout from "../components/Layout";

//! Components
import Cards from "../components/Cards";
import Howdy from "../components/Howdy";
import Leads from "../components/Leads";
import Range from "../components/Range";

const Dashboard = ({ page }) => {
  const [months, setMonths] = useState([]);
  const [leads, setLeads] = useState([]);
  const [currentMonth, setCurrentMonth] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`https://warmleads.vercel.app/api`);
      const data = await response.json();

      setLoading(false);
      setMonths(data && data.months);
      setCurrentMonth(data && data.months[0]);
      setLeads(data.leads);
    };

    fetchData();
  }, []);

  const changeCurrentMonth = (month) => {
    setCurrentMonth(month);
  };

  return (
    <Layout currentPage={page}>
      <Howdy />
      <Range
        currentMonth={currentMonth}
        months={months}
        changeCurrentMonth={changeCurrentMonth}
      />
      <div className="content">
        <div className="section grid grid-cols-3 gap-4 p-6">
          <Cards
            count={currentMonth.leads}
            label="Leads (Last 30 Days)"
            icon="Users"
            options={[{ label: "View monthly leads", link: "monthly" }]}
          />
          <Cards
            count={currentMonth.emailsOpened}
            label="Leads (Opened email)"
            icon="Mail"
            progress="+49%"
          />
          <Cards
            count={currentMonth.mailsPerLead}
            label="Mails sent per Lead"
            icon="Send"
            progress="-19%"
          />
        </div>
      </div>
      <Leads
        header="Interested Leads"
        leads={leads.filter((f) => f.isLeadHot)}
        loading={loading}
      />
    </Layout>
  );
};

export default Dashboard;
