import React, { useEffect, useState } from "react";

import Leads from "../components/Leads";
import Layout from "../components/Layout";
import Range from "../components/Range";

const Container = ({ page }) => {
  const [months, setMonths] = useState([]);
  const [leads, setLeads] = useState([]);
  const [currentMonth, setCurrentMonth] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`https://warmleads.vercel.app/api`);
      const data = await response.json();

      setLoading(false);
      setMonths(data && data.months);
      setCurrentMonth(data && data.months[0]);
      setLeads(data.leads);
    };

    fetchData();
  }, []);

  const changeCurrentMonth = (month) => {
    setCurrentMonth(month);
  };

  return (
    <Layout currentPage={page}>
      <div className="content">
        <Range
          currentMonth={currentMonth}
          months={months}
          changeCurrentMonth={changeCurrentMonth}
        />
        <Leads
          leads={leads.filter((f) => f.month === currentMonth.key)}
          loading={loading}
        />
      </div>
    </Layout>
  );
};

export default Container;
