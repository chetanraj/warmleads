import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import "./index.css";
import reportWebVitals from "./reportWebVitals";

import Dashboard from "./pages/Dashboard";
import Monthly from "./pages/Monthly";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Dashboard page="dashboard" />,
  },
  {
    path: "/monthly",
    element: <Monthly page="monthly" />,
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
